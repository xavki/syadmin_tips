#!/bin/bash

###############################################################
#
#  DESCRIPTION: nombre de fichiers modifiés par jour
###############################################################


find . -type f -exec ls -la --full-time {} \; | grep -v total |  cut -f6 -d " " | sort | uniq -c
