#!/bin/bash

###############################################################
#  DESCRIPTION: lancer des notifications sur votre écran
###############################################################

# installation (déjà sur ubuntu)
sudo apt-get install libnotify-bin


# exemple
sleep 15s && notify-send "Il est temps de faire une pause"

# deux lignes
notify-send 'Bonjour !' 'Je suis Xavier'

# smiley inquiet
notify-send 'Vous avez des erreurs dans votre script' 'Vous devez consulter les logs' -u critical -i face-worried

#checked icon
notify-send 'La mise à jour a été réalisé' 'avec succés !' -u normal -t 7500 -i checkbox-checked-symbolic
