#!/bin/bash

###############################################################
#
#  AUTEUR:   Xavier
#
#  DESCRIPTION: install mdp slides
#  source : https://github.com/visit1985/mdp
###############################################################

git clone https://github.com/visit1985/mdp.git
cd mdp
make
make install
mdp sample.md
