#!/bin/bash

###############################################################
#  DESCRIPTION: commande aléatoire et première ligne de man
###############################################################


man $(ls /usr/bin | shuf -n 1)| sed -n "/^NAME/ { n;p;q}"
